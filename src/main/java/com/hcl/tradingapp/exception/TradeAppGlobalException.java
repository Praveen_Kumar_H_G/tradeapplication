package com.hcl.tradingapp.exception;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TradeAppGlobalException extends RuntimeException {


	private static final long serialVersionUID = 1L;
	private String message;
	private String code;

	public TradeAppGlobalException(String message) {
		super(message);
	}
}
