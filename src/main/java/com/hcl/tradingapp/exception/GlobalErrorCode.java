package com.hcl.tradingapp.exception;

public class GlobalErrorCode {
	public static final String ERROR_RESOURCE_NOT_FOUND = "EX10001";
	public static final String ERROR_RESOURCE_CONFLICT_EXISTS = "EX10002";
	public static final String ERROR_UNAUTHOURIZED_USER = "EX10003";
	public static final String ERROR_INSUFFICIENT_FUND = "EX10004";

	private GlobalErrorCode() {
		super();
	}
}