package com.hcl.tradingapp.exception;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ErrorResponse {
//    private HttpStatus code;
	private String code;
    private String message;
}
