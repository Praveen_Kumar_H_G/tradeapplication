package com.hcl.tradingapp.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
public class CashAccount {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long cashAccountId;
	@OneToOne
	private User user;
	private Double balance;

}
