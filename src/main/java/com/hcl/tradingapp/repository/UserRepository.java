package com.hcl.tradingapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.tradingapp.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
