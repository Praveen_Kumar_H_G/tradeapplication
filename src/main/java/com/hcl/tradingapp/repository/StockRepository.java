package com.hcl.tradingapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.tradingapp.entity.Stock;

public interface StockRepository extends JpaRepository<Stock, Long> {

}
